/*
 * Copyright 2019-2020 Collabora Ltd.
 * Copyright 2020 Simon McVittie
 * SPDX-License-Identifier: Apache-2.0
 * (see "Apache" in debian/copyright)
 */

#include <roaring/roaring.h>

int
main (int argc, char **argv)
{
  roaring_bitmap_t *bitmap = roaring_bitmap_create();
  roaring_bitmap_free(bitmap);
  return 0;
}
